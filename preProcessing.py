import numpy as np
import cv2
import os
import imutils



def visual_aid():
    cwd = os.getcwd()
    imagesDir = os.path.join(cwd, 'TrainData', 'ct', 'train')
    imagesNamesList = os.listdir(imagesDir)
    segDir = os.path.join(cwd, 'TrainData', 'seg', 'train')
    segNamesList = os.listdir(segDir)
    saveDir = os.path.join(cwd, 'TrainData', 'visual', 'train')

    for imageName, segName in zip(imagesNamesList, segNamesList):
        image = cv2.imread(os.path.join(imagesDir, imageName), cv2.IMREAD_GRAYSCALE)
        seg = cv2.imread(os.path.join(segDir, segName), cv2.IMREAD_GRAYSCALE)
        liver = np.argwhere(seg == 127)
        boxes = image_contours(seg)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        if len(liver) > 0:
            liverXrange = np.array([np.min(liver[:, 1]), np.max(liver[:, 1])])
            liverYrange = np.array([np.min(liver[:, 0]), np.max(liver[:, 0])])
            cv2.rectangle(image, (liverXrange[0] - 1, liverYrange[0] - 1),
                          (liverXrange[1] + 1, liverYrange[1] + 1), (127, 0, 0), 2)
        if len(boxes) > 0:
            for box in boxes:
                cv2.rectangle(image, box['p1'], box['p2'], (0, 0, 127), 1)
        cv2.imwrite(os.path.join(saveDir, imageName), image)
        cv2.imshow('image', image)
        cv2.waitKey(1)


def image_contours(img):

    thresh = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY)[1]
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    boxes = []
    box = {}
    # loop over the contours
    for c in cnts:
        (x, y, w, h) = cv2.boundingRect(c)
        box['p1'] = (x, y)
        box['p2'] = (x + w, y + h)
        boxes.append(box.copy())

    return boxes

visual_aid()
